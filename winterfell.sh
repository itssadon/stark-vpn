#!/bin/bash
echo "🤓  Welcome to StarkVPN' 🐺  Yo!!!"

echo 'I need Administrative Privileges to do ma thang. Do you want to continue? Enter (Y/n)'
read accessDecision
if [ $accessDecision == 'Y' ]
then
  if [ "$UID" -ne 0 ]
    then
      exec sudo -S "$0" "$@"
  fi

  echo "Please choose setup option: (1) Auto (2) Manual"
  read setupOption
  if [ "$setupOption" -eq 1 ]
  then
    # Auto setup here
    wget https://git.io/vpnsetup -O vpnsetup.sh && sudo sh vpnsetup.sh
  else if [ "$setupOption" -eq 2]
  then
    # Manual setup here
    apt update
    apt upgrade -y

    # Install xl2tpd
    apt install xl2tpd -y

    # Install openswan and other dependencies
    sudo apt install openswan ppp -y

    # Configure ipsec
    cat ipsec.conf > /etc/ipsec.conf
    cat ipsec.secrets > /etc/ipsec.secrets
    /etc/init.d/ipsec start
    
    # Verify config
    ipsec Verify

    # ipsec vpn
    cat ipsec.vpn > /etc/init.d/ipsec.vpn

    chmod 755 ipsec.vpn

    \#update-rc.d -f ipsec remove

    \#update-rc.d ipsec.vpn defaults

    cat xl2tpd.conf > /etc/xl2tpd/xl2tpd.conf

    cat l2tp-secrets > /etc/xl2tpd/l2tp-secrets

    cat options.xl2tpd > /etc/ppp/options.xl2tpd

    cat chap-secrets > /etc/ppp/chap-secrets

    echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf

    sysctl -p

    sudo /etc/init.d/ipsec.vpn restart
    sudo /etc/init.d/xl2tpd restart
  else
    echo "Bye... Come back when you are ready 🙄"
  fi
else
  echo "Bye... Come back when you are ready 🙄"
fi